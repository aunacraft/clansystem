package net.aunacraft.clansystem.npc;

import net.aunacraft.api.bukkit.util.LocationUtil;
import net.aunacraft.api.database.DatabaseHandler;
import org.bukkit.Location;

import java.sql.SQLException;
import java.util.function.Consumer;

public class NPCLocationProvider {

    private DatabaseHandler databaseHandler;

    public NPCLocationProvider(final DatabaseHandler databaseHandler) {
        this.databaseHandler = databaseHandler;
        databaseHandler.createBuilder("CREATE TABLE IF NOT EXISTS clan_settings (key TEXT, value TEXT)").updateSync();
    }

    public void getLocationFromDatabaseAsync(final Consumer<Location> callback) {
        this.databaseHandler.createBuilder("SELECT * FROM clan_settings WHERE key = ?").addObjects("location").queryAsync(cachedRowSet -> {
            try {
                if (cachedRowSet.next()) {
                    callback.accept(LocationUtil.locationFromString(cachedRowSet.getString("value")));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void setLocationToDatabase(final Location location, final Runnable callback) {
        this.databaseHandler.createBuilder("UPDATE clan_settings SET value = ? WHERE key = ?").addObjects(LocationUtil.locationToString(location), "location").updateAsync(callback);
    }
}
