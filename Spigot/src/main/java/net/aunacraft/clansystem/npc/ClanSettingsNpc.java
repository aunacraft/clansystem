package net.aunacraft.clansystem.npc;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.bukkit.npc.base.NPCBase;
import net.aunacraft.api.bukkit.npc.builder.NPCBuilder;
import net.aunacraft.api.util.SkinFetcher;
import net.aunacraft.clansystem.ClanSystem;
import net.aunacraft.clansystem.clan.ClanPlayer;
import net.aunacraft.clansystem.clan.mysql.SQLManagement;
import net.aunacraft.clansystem.gui.settings.SettingsIndexGui;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class ClanSettingsNpc {

    public static final ArrayList<Player> lastClicked = new ArrayList<>();
    private NPCBase base;

    /*
        NPC CREATION/INITIALIZE METHOD
     */
    public void init(final Location location) {
        NPCBase npc = new NPCBuilder(ClanSystem.getInstance())
                .setInteractListener(npcInteractEvent -> {
                    npcInteractEvent.getPlayer().sendMessage("Hai!!!!");
                    if (lastClicked.contains(npcInteractEvent.getPlayer())) {
                        npcInteractEvent.getPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(npcInteractEvent.getPlayer(), "clan.settingsnpc.waitmessage"));
                    } else {
                        SQLManagement.existsClanPlayer(AunaAPI.getApi().getPlayer(npcInteractEvent.getPlayer().getUniqueId()).getUuid().toString(), exists -> {
                            if (exists)
                                Bukkit.getScheduler().scheduleSyncDelayedTask(ClanSystem.getInstance(), () -> {
                                    new ClanPlayer(AunaAPI.getApi().getPlayer(npcInteractEvent.getPlayer().getUniqueId()).getUuid().toString(), cp -> new SettingsIndexGui(cp.toAunaPlayer(), cp.getClan()).open(npcInteractEvent.getPlayer()));
                                });
                            else {
                                lastClicked.add(npcInteractEvent.getPlayer());
                                Bukkit.getScheduler().runTaskLaterAsynchronously(ClanSystem.getInstance(), () -> lastClicked.remove(npcInteractEvent.getPlayer()), 20 * 3);
                            }
                        });
                    }
                })
                .setLocation(location)
                .setLookAtPlayer(true)
                .setVisibleType(NPCBase.VisibleType.ALL)
                .setNameKey("clan.settingsnpc.name")
                .setSkin(SkinFetcher.getSkinFromName("Pahuul"))
                .build();
        this.base = npc;
    }

    /*
        WOULD SPAWN THE NPC FOR AN PLAYER IF NECESSARY
     */
    @Deprecated
    public void spawnForPlayer(final Player player) {
        if (base != null) {
            base.getNPC(player).spawn();
        }
    }
}
