package net.aunacraft.clansystem;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.backend.message.MessageService;
import net.aunacraft.api.database.DatabaseHandler;
import net.aunacraft.api.database.config.impl.SpigotDatabaseConfig;
import net.aunacraft.api.event.impl.PlayerRegisterEvent;
import net.aunacraft.clansystem.commands.AunaClanCommand;
import net.aunacraft.clansystem.listeners.PluginMessageListener;
import net.aunacraft.clansystem.listeners.RegisterListener;
import net.aunacraft.clansystem.npc.ClanSettingsNpc;
import net.aunacraft.clansystem.npc.NPCLocationProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Locale;

public class ClanSystem extends JavaPlugin {

    private static ClanSystem instance;
    private MessageService messageService;
    private DatabaseHandler databaseHandler;
    private NPCLocationProvider npcLocationProvider;
    private ClanSettingsNpc npc;

    public static ClanSystem getInstance() {
        return instance;
    }

    public NPCLocationProvider getNpcLocationProvider() {
        return npcLocationProvider;
    }

    public MessageService getMessageService() {
        return messageService;
    }

    public DatabaseHandler getDatabaseHandler() {
        return databaseHandler;
    }

    public ClanSettingsNpc getNpc() {
        return npc;
    }

    public void setNpc(ClanSettingsNpc npc) {
        this.npc = npc;
    }

    @Override
    public void onEnable() {
        instance = this;
        // API STUFF
        this.databaseHandler = new DatabaseHandler(new SpigotDatabaseConfig(this));
        this.messageService = AunaAPI.getApi().getMessageService();
        this.messageService.applyPrefix("clan", "clan.prefix");
        // CLAN STUFF
        this.initCommandsAndEvents();
        this.databaseHandler.createBuilder("CREATE TABLE IF NOT EXISTS Clans(tag TEXT, name TEXT, color TEXT, level INT, bankAmount BIGINT, ranks TEXT, state TEXT, font TEXT);").updateSync();
        this.databaseHandler.createBuilder("CREATE TABLE IF NOT EXISTS clan_player(uuid TEXT, tag TEXT, ranks TEXT);").updateSync();
        this.npcLocationProvider = new NPCLocationProvider(this.databaseHandler);
        new PluginMessageListener();
        this.npcLocationProvider.getLocationFromDatabaseAsync(location -> {
            if (location != null) {
                this.npc = new ClanSettingsNpc();
                this.npc.init(location);
            } else {
                this.getServer().getLogger().info("[CLAN] The Settings NPC´s Location isnt setted yet. Please set it!");
            }
        });
    }

    public String getFormatedSuffix(final String color, final String tag) {
        return "§8 | " + color + tag.toUpperCase(Locale.ROOT);
    }

    private void initCommandsAndEvents() {
        //COMMANDS
        //this.getCommand("clan").setExecutor(new ClanCommand()); OLD COMMAND REGISTRATION
        AunaAPI.getApi().registerCommand(new AunaClanCommand());
        //LISTENER
        AunaAPI.getApi().registerEventListener(PlayerRegisterEvent.class, RegisterListener::onRegister);
    }

    @Override
    public void onDisable() {

    }
}
