package net.aunacraft.clansystem.commands.autocompletion;

import net.aunacraft.api.commands.autocompleetion.AutoCompletionHandler;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class PlayerCompletion implements AutoCompletionHandler<Player> {
    @Override
    public List<String> getCompletions() {
        List<String> playerNames = new ArrayList<>();
        Bukkit.getOnlinePlayers().forEach(player -> playerNames.add(player.getName()));
        return playerNames;
    }

    @Override
    public boolean isValid(String s) {
        return Bukkit.getPlayer(s) != null;
    }

    @Override
    public Player convertString(String s) {
        return Bukkit.getPlayer(s);
    }
}
