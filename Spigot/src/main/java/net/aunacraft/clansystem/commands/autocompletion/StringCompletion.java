package net.aunacraft.clansystem.commands.autocompletion;

import com.google.common.collect.Lists;
import net.aunacraft.api.commands.autocompleetion.AutoCompletionHandler;

import java.util.List;

public class StringCompletion implements AutoCompletionHandler<String> {
    @Override
    public List<String> getCompletions() {
        return Lists.newArrayList();
    }

    @Override
    public boolean isValid(String s) {
        return true;
    }

    @Override
    public String convertString(String s) {
        return s;
    }
}
