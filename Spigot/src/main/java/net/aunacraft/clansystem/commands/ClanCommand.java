package net.aunacraft.clansystem.commands;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.clansystem.ClanSystem;
import net.aunacraft.clansystem.clan.Clan;
import net.aunacraft.clansystem.clan.ClanPlayer;
import net.aunacraft.clansystem.clan.mysql.SQLManagement;
import net.aunacraft.clansystem.clan.permission.impl.DefaultOwnerRank;
import net.aunacraft.clansystem.gui.settings.SettingsIndexGui;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@Deprecated
public class ClanCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) return true;
        commandSender.sendMessage("1");
        AunaPlayer player = AunaAPI.getApi().getPlayer(((Player) commandSender).getUniqueId());
        if (args.length == 0) {
            this.printHelp(player.toBukkitPlayer());
            return true;
        }

        SQLManagement.existsClanPlayer(player.getUuid().toString(), exists -> {
            commandSender.sendMessage("2");
            if (args.length == 1) {
                commandSender.sendMessage("3");
                if (args[0].equalsIgnoreCase("settings")) {
                    commandSender.sendMessage("4");
                    if (exists) {
                        commandSender.sendMessage("5");
                        new ClanPlayer(player.getUuid().toString(), cp -> {
                            commandSender.sendMessage("6");
                            Bukkit.getScheduler().scheduleSyncDelayedTask(ClanSystem.getInstance(), () -> new SettingsIndexGui(cp.toAunaPlayer(), cp.getClan()).open(cp.toBukkitPlayer()));
                        });
                    } else {
                        player.toBukkitPlayer().sendMessage("Mau!");
                    }
                } else if (args[0].equalsIgnoreCase("help")) {
                    this.printHelp(player.toBukkitPlayer());
                } else {
                    this.printHelp(player.toBukkitPlayer());
                }
            } else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("info")) {
                    SQLManagement.existsClan(args[1], aBoolean -> {
                        if (aBoolean) {
                            new Clan(args[1], clan -> {
                                player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.info.1", clan.getName()));
                                player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.info.2", "Name: " + clan.getName()));
                                player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.info.2", "Tag: " + clan.getTag()));
                                player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.info.2", "Level: " + clan.getLevel()));
                                player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.info.2", "State: " + clan.getClanState().toString()));
                                clan.getPlayersAsync(array -> {
                                    player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.info.2", "Member: " + array.size() + "/" + clan.getMaxPlayers()));
                                });
                            });
                        } else {
                            player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.clannotexists"));
                        }
                    });
                } else if (args[0].equalsIgnoreCase("join")) {
                    SQLManagement.existsClan(args[1], aBoolean -> {
                        if (aBoolean) {
                            new Clan(args[1], clan -> {
                                if (clan.getClanState().equals(Clan.ClanState.OPEN)) {
                                    clan.getPlayersAsync(playerArray -> {
                                        if (clan.getMaxPlayers() >= playerArray.size()) {
                                            if (exists) {
                                                new ClanPlayer(player.getUuid().toString(), clanPlayer -> {
                                                    final Clan oldClan = clanPlayer.getClan();
                                                    clanPlayer.setClanKey(clan.getTag());
                                                    clanPlayer.setRankName(clan.getDefaultRank().getGroupName());
                                                    clanPlayer.pushChange();
                                                    clanPlayer.updateSuffix(player);
                                                    player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.join.success"));
                                                });
                                            } else {
                                                ClanPlayer clanPlayer = new ClanPlayer(clan.getTag(), player.getUuid().toString(), clan.getDefaultRank().getGroupName());
                                                clanPlayer.createClanPlayer();
                                                player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.join.success"));
                                            }
                                        } else {
                                            player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.join.full"));
                                        }
                                    });
                                } else {
                                    player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.join.notopenstate"));
                                }
                            });
                        } else {
                            player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.clannotexists"));
                        }
                    });
                } else if (args[0].equalsIgnoreCase("invite")) {

                }
            } else if (args.length == 3) {
                commandSender.sendMessage("3");
                // /clan create HUAN HUAN
                if (args[0].equalsIgnoreCase("create")) {
                    if (isValidString(args[2]) && isValidString(args[1])) {
                        commandSender.sendMessage("4");
                        if (args[2].length() <= 5) {
                            final Clan clanToCreate = new Clan(args[1], args[2], Clan.DEFAULT_COLOR, Clan.DEFAULT_LEVEL);
                            clanToCreate.createClan(() -> {
                                commandSender.sendMessage("5");
                                if (exists) {
                                    commandSender.sendMessage("JNDKJNAKJDHNKJDND");
                                    new ClanPlayer(player.getUuid().toString(), cp -> {
                                        cp.setRankName(new DefaultOwnerRank().getGroupName());
                                        cp.setClanKey(clanToCreate.getTag());
                                        cp.pushChange();
                                        cp.updateSuffix();
                                    });
                                } else {
                                    System.out.println("6");
                                    ClanPlayer clanPlayer = new ClanPlayer(clanToCreate.getTag(), player.getUuid().toString(), new DefaultOwnerRank().getGroupName());
                                    clanPlayer.createClanPlayer();
                                    clanPlayer.updateSuffix();
                                }
                                ClanSystem.getInstance().getMessageService().sendMessage(player, "clan.command.create.success");
                            });
                        } else {
                            player.sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.tomuchcharsattag"));
                        }
                    } else {
                        printHelp(player.toBukkitPlayer());
                    }
                }
            } else printHelp(player.toBukkitPlayer());
        });
        /*
if(args[0].equalsIgnoreCase("create")){
                    if(isValidString(args[2]) && isValidString(args[1])){
                        if(args[2].length() <= 5){
                            final Clan clanToCreate = new Clan(args[1], args[2], Clan.DEFAULT_COLOR, Clan.DEFAULT_LEVEL);
                            clanToCreate.createClan(null);
                            if(exists){
                                SQLManagement.setClanPlayerValues(player.getUuid().toString(), clanToCreate.getTag(), new DefaultOwnerRank().getGroupName());
                            }else{
                                clanPlayer = new ClanPlayer(clanToCreate.getTag(), player.getUuid().toString(), new DefaultOwnerRank().getGroupName());
                                clanPlayer.createClanPlayer();
                            }
                            clanPlayer.updateSuffix(player);
                            ClanSystem.getInstance().getMessageService().sendMessage(player, "clan.command.create.success");
                        }else{
                            player.sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.tomuchcharsattag"));
                        }
                    }else{
                        player.sendMessage("Falsche dinghesms"); //TODO
                    }
                }
        /clan help - Help Menu TODO
        /clan bank add <money> - Deposits Money TODO
        /clan bank remove <money> - Withdraws Mondey TODO
        /clan bank info - Saw Info about the Bank TODO
        /clan settings - Öffnet das Settings Menu TODO
        /clan join <Clan> - Joins a Clan FINISHED
        /clan invite <Player> - Invites a Player TODO
        /clan accept <Clan> - Accepts a Invitation TODO
        /clan deny <Clan> - Denys a Invitation TODO
        /clan info <Clan> - Gets Information about a Clan FINISHED
        /clan top - You saw the top clans TODO

         */
        return false;
    }

    private boolean isValidString(String str) {
        for (char c : str.toCharArray()) {
            String string = String.valueOf(c);
            if (!string.matches("[a-zA-Z]")) {
                return false;
            }
        }
        return true;
    }

    private void printHelp(final Player player) {
        player.sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.command.help1"));
        player.sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.command.help2"));
    }
}
