package net.aunacraft.clansystem.commands;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.Command;
import net.aunacraft.api.commands.builder.CommandBuilder;
import net.aunacraft.api.commands.builder.ParameterBuilder;
import net.aunacraft.clansystem.ClanSystem;
import net.aunacraft.clansystem.clan.Clan;
import net.aunacraft.clansystem.clan.ClanPlayer;
import net.aunacraft.clansystem.clan.invites.InviteSession;
import net.aunacraft.clansystem.clan.mysql.SQLManagement;
import net.aunacraft.clansystem.clan.permission.impl.DefaultOwnerRank;
import net.aunacraft.clansystem.clan.permission.provider.ClanPermission;
import net.aunacraft.clansystem.commands.autocompletion.PlayerCompletion;
import net.aunacraft.clansystem.commands.autocompletion.StringCompletion;
import net.aunacraft.clansystem.gui.settings.SettingsIndexGui;
import net.aunacraft.clansystem.npc.ClanSettingsNpc;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class AunaClanCommand implements AunaCommand {
    @Override
    public Command createCommand() {
        return CommandBuilder.beginCommand("clan")
                .subCommand(CommandBuilder.beginCommand("help").handler((aunaPlayer, commandContext, strings) -> {

                }).build())
                .subCommand(CommandBuilder.beginCommand("settings").handler((aunaPlayer, commandContext, strings) -> {
                    SQLManagement.existsClanPlayer(aunaPlayer.getUuid().toString(), exists -> {
                        if (exists) {
                            new ClanPlayer(aunaPlayer.getUuid().toString(), cp -> {
                                Bukkit.getScheduler().scheduleSyncDelayedTask(ClanSystem.getInstance(),
                                        () -> new SettingsIndexGui(cp.toAunaPlayer(), cp.getClan()).open(cp.toBukkitPlayer()));
                            });
                        } else {
                            aunaPlayer.toBukkitPlayer().sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.clanplayerneeded"));
                        }
                    });

                }).build())
                .subCommand(CommandBuilder.beginCommand("top").handler((aunaPlayer, commandContext, strings) -> {

                }).build())
                .subCommand(CommandBuilder.beginCommand("info")
                        .parameter(ParameterBuilder.beginParameter("Clan").required().autoCompletionHandler(new StringCompletion()).build())
                        .handler((player, commandContext, args) -> {
                            SQLManagement.existsClan(commandContext.getParameterValue("Clan", String.class), aBoolean -> {
                                if (aBoolean) {
                                    new Clan(args[1], clan -> {
                                        player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.info.1", clan.getName()));
                                        player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.info.2", "Name: §e" + clan.getName()));
                                        player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.info.2", "Tag: §e" + clan.getTag()));
                                        player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.info.2", "Level: §e" + clan.getLevel()));
                                        player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.info.2", "State: §e" + clan.getClanState().toString()));
                                        clan.getPlayersAsync(array -> {
                                            player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.info.2", "Member: " + array.size() + "/" + clan.getMaxPlayers()));
                                        });
                                    });
                                } else {
                                    player.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.command.clannotexists"));
                                }
                            });
                        }).build())
                .subCommand(CommandBuilder.beginCommand("invite")
                        .parameter(ParameterBuilder.beginParameter("Player").required().autoCompletionHandler(new PlayerCompletion()).build())
                        .handler((aunaPlayer, commandContext, strings) -> {
                            SQLManagement.existsClanPlayer(aunaPlayer.getUuid().toString(), exists -> {
                                if (exists) {
                                    new ClanPlayer(aunaPlayer.getUuid().toString(), cp -> {
                                        InviteSession session = new InviteSession(cp, strings[0]);
                                        session.start();
                                    });
                                } else {
                                    aunaPlayer.toBukkitPlayer().sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.clanplayerneeded"));
                                }
                            });
                        }).build())
                .subCommand(CommandBuilder.beginCommand("join")
                        .parameter(ParameterBuilder.beginParameter("Player").required().autoCompletionHandler(new PlayerCompletion()).build())
                        .handler((aunaPlayer, commandContext, args) -> {
                            SQLManagement.existsClanPlayer(aunaPlayer.getUuid().toString(), exists -> {
                                SQLManagement.existsClan(args[1], aBoolean -> {
                                    if (aBoolean) {
                                        new Clan(args[1], clan -> {
                                            if (clan.getClanState().equals(Clan.ClanState.OPEN)) {
                                                clan.getPlayersAsync(playerArray -> {
                                                    if (clan.getMaxPlayers() >= playerArray.size()) {
                                                        if (exists) {
                                                            aunaPlayer.toBukkitPlayer().sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.havetoleaveclan"));
                                                        } else {
                                                            ClanPlayer clanPlayer = new ClanPlayer(clan.getTag(), aunaPlayer.getUuid().toString(), clan.getDefaultRank().getGroupName());
                                                            clanPlayer.createClanPlayer();
                                                            aunaPlayer.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.join.success"));
                                                        }
                                                    } else {
                                                        aunaPlayer.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.join.full"));
                                                    }
                                                });
                                            } else {
                                                aunaPlayer.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.join.notopenstate"));
                                            }
                                        });
                                    } else {
                                        aunaPlayer.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.clannotexists"));
                                    }
                                });
                            });
                        }).build())
                .subCommand(CommandBuilder.beginCommand("create")
                        .parameter(ParameterBuilder.beginParameter("Name").autoCompletionHandler(new StringCompletion()).required().build())
                        .parameter(ParameterBuilder.beginParameter("Tag").autoCompletionHandler(new StringCompletion()).required().build())
                        .handler((aunaPlayer, commandContext, args) -> {
                            SQLManagement.existsClanPlayer(aunaPlayer.getUuid().toString(), exists -> {
                                if (isValidString(args[2]) && isValidString(args[1])) {
                                    if (args[2].length() <= 5) {
                                        final Clan clanToCreate = new Clan(args[1], args[2], Clan.DEFAULT_COLOR, Clan.DEFAULT_LEVEL);
                                        clanToCreate.createClan(() -> {
                                            if (exists) {
                                                new ClanPlayer(aunaPlayer.getUuid().toString(), cp -> {
                                                    cp.setRankName(new DefaultOwnerRank().getGroupName());
                                                    cp.setClanKey(clanToCreate.getTag());
                                                    cp.pushChange();
                                                    cp.updateSuffix();
                                                });
                                            } else {
                                                ClanPlayer clanPlayer = new ClanPlayer(clanToCreate.getTag(), aunaPlayer.getUuid().toString(), new DefaultOwnerRank().getGroupName());
                                                clanPlayer.createClanPlayer();
                                                clanPlayer.updateSuffix();
                                            }
                                            ClanSystem.getInstance().getMessageService().sendMessage(aunaPlayer.toBukkitPlayer(), "clan.command.create.success");
                                        });
                                    } else {
                                        aunaPlayer.toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.tomuchcharsattag"));
                                    }
                                } else {
                                    printHelp(aunaPlayer.toBukkitPlayer());
                                }
                            });
                        }).build())
                .subCommand(CommandBuilder.beginCommand("delete")
                        .subCommand(CommandBuilder.beginCommand("confirm").handler((aunaPlayer, commandContext, strings) -> {
                            SQLManagement.existsClanPlayer(aunaPlayer.getUuid().toString(), exists -> {
                                if (exists) {
                                    new ClanPlayer(aunaPlayer.getUuid().toString(), clanPlayer -> {
                                        if (clanPlayer.hasPermission(ClanPermission.DELETE)) {
                                            clanPlayer.getClan().delete(() -> {
                                                aunaPlayer.toBukkitPlayer().sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.deletesuccess"));
                                            });
                                        } else {
                                            aunaPlayer.toBukkitPlayer().sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.noclanperm"));
                                        }
                                    });
                                } else {
                                    aunaPlayer.toBukkitPlayer().sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.clanplayerneeded"));
                                }
                            });
                        }).build()).handler((aunaPlayer, commandContext, strings) -> {
                            SQLManagement.existsClanPlayer(aunaPlayer.getUuid().toString(), exists -> {
                                if (exists) {
                                    new ClanPlayer(aunaPlayer.getUuid().toString(), clanPlayer -> {
                                        if (clanPlayer.hasPermission(ClanPermission.DELETE)) {
                                            TextComponent component = new TextComponent(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.message.delete", clanPlayer.getClan().getName()));
                                            component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/clan delete confirm"));
                                            aunaPlayer.toBukkitPlayer().spigot().sendMessage(component);
                                        } else {
                                            aunaPlayer.toBukkitPlayer().sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.noclanperm"));
                                        }
                                    });
                                } else {
                                    aunaPlayer.toBukkitPlayer().sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.clanplayerneeded"));
                                }
                            });
                        }).aliases("del").build())
                .subCommand(CommandBuilder.beginCommand("leave")
                        .subCommand(CommandBuilder.beginCommand("confirm")
                                .handler((aunaPlayer, commandContext, strings) -> {
                                    SQLManagement.existsClanPlayer(aunaPlayer.getUuid().toString(), exists -> {
                                        if (exists) {
                                            new ClanPlayer(aunaPlayer.getUuid().toString(), clanPlayer -> {
                                                if (!clanPlayer.hasPermission(ClanPermission.DELETE)) {
                                                    clanPlayer.getClan().sendClanBroadcast("clan.broadcast.leave|" + aunaPlayer.getName());
                                                    clanPlayer.delete();
                                                    clanPlayer.deleteSuffix();
                                                    aunaPlayer.toBukkitPlayer().sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.message.leavesuccess"));
                                                } else {
                                                    aunaPlayer.toBukkitPlayer().sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.leave.tohighrank"));
                                                }
                                            });
                                        } else {
                                            aunaPlayer.toBukkitPlayer().sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.clanplayerneeded"));
                                        }
                                    });
                                }).build())
                        .handler((aunaPlayer, commandContext, strings) -> {
                            SQLManagement.existsClanPlayer(aunaPlayer.getUuid().toString(), exists -> {
                                if (exists) {
                                    new ClanPlayer(aunaPlayer.getUuid().toString(), clanPlayer -> {
                                        if (!clanPlayer.hasPermission(ClanPermission.DELETE)) {
                                            TextComponent component = new TextComponent(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.message.leave", clanPlayer.getClan().getName()));
                                            component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/clan leave confirm"));
                                            aunaPlayer.toBukkitPlayer().spigot().sendMessage(component);
                                        } else {
                                            aunaPlayer.toBukkitPlayer().sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.leave.tohighrank"));
                                        }
                                    });
                                } else {
                                    aunaPlayer.toBukkitPlayer().sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(aunaPlayer.toBukkitPlayer(), "clan.command.clanplayerneeded"));
                                }
                            });
                        }).build())
                .subCommand(CommandBuilder.beginCommand("setnpc")
                        .permission("clansystem.setnpc")
                        .handler((aunaPlayer, commandContext, strings) -> {
                            final Location location = aunaPlayer.toBukkitPlayer().getLocation().clone();
                            ClanSystem.getInstance().getNpcLocationProvider().setLocationToDatabase(location, () -> {
                                ClanSystem.getInstance().setNpc(new ClanSettingsNpc());
                                ClanSystem.getInstance().getNpc().init(location);
                                aunaPlayer.sendMessage("clan.command.setnpc.successfull");
                            });
                        }).build())
                .build();
    }

    private boolean isValidString(String str) {
        for (char c : str.toCharArray()) {
            String string = String.valueOf(c);
            if (!string.matches("[a-zA-Z]")) {
                return false;
            }
        }
        return true;
    }

    private void printHelp(final Player player) {
        player.sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.command.help1"));
        player.sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.command.help2"));
    }

    /*

        COMMAND-TODO-List

        /clan help - Help Menu TODO
        /clan bank add <money> - Deposits Money TODO
        /clan bank remove <money> - Withdraws Mondey TODO
        /clan bank info - Saw Info about the Bank TODO
        /clan settings - Öffnet das Settings Menu FINISHED
        /clan join <Clan> - Joins a Clan FINISHED
        /clan invite <Player> - Invites a Player FINISHED
        /clan accept <Clan> - Accepts a Invitation TODO
        /clan deny <Clan> - Denys a Invitation TODO
        /clan info <Clan> - Gets Information about a Clan FINISHED
        /clan top - You saw the top clans TODO
        /clan delete - Deletes the Clan FINISHED
        /clan leave - Leaves the CLan FINAISHED
     */
}
