package net.aunacraft.clansystem.clan.permission.impl;

import net.aunacraft.clansystem.clan.permission.ClanRank;
import net.aunacraft.clansystem.clan.permission.provider.ClanPermission;
import net.aunacraft.clansystem.clan.permission.provider.PermissionProvider;
import org.bukkit.ChatColor;

import java.util.Arrays;

public class DefaultOwnerRank extends ClanRank {
    public DefaultOwnerRank() {
        super(new PermissionProvider(), ClanRank.MAX_RANK_LEVEL, "Owner", "Owner", ChatColor.RED.toString());
        Arrays.asList(ClanPermission.values()).forEach(permission -> this.getPermissionProvider().setPermission(permission, true));
    }
}
