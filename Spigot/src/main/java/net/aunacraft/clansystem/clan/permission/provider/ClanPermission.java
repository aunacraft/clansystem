package net.aunacraft.clansystem.clan.permission.provider;

import org.bukkit.inventory.ItemStack;

public enum ClanPermission {

    CHANGE_STATE(null),
    DEPOSIT_BANK(null),
    WITHDRAW_BANK(null),
    EDIT_RANKS(null),
    KICK(null),
    BAN(null),
    DELETE(null);

    private final ItemStack icon;

    ClanPermission(ItemStack item) {
        this.icon = item;
    }

    public ItemStack getIcon() {
        return icon;
    }
}
