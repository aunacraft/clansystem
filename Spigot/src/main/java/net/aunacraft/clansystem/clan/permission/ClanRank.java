package net.aunacraft.clansystem.clan.permission;

import com.google.gson.Gson;
import lombok.Getter;
import lombok.Setter;
import net.aunacraft.clansystem.clan.permission.provider.PermissionProvider;

@Getter
@Setter
public class ClanRank {

    public static final int MAX_RANK_LEVEL = 7;
    private final PermissionProvider permissionProvider;
    private int rankLevel;
    private String groupName;
    private String displayName;
    private String color;

    /**
     * Initialize the ClanRank
     *
     * @param permissionProvider
     * @param rankLevel
     * @param groupName
     * @param displayName
     * @param color
     */
    public ClanRank(final PermissionProvider permissionProvider, final int rankLevel, final String groupName, final String displayName, final String color) {
        this.permissionProvider = permissionProvider;
        this.rankLevel = rankLevel;
        this.groupName = groupName;
        this.displayName = displayName;
        this.color = color;
    }

    /**
     * Unserializes the Serialized String
     *
     * @param from
     * @return A instance of ClanRank unserialized from
     */
    public static ClanRank fromString(final String from) {
        return new Gson().fromJson(from, ClanRank.class);
    }

    /**
     * @return Serialized instance of this
     */
    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
