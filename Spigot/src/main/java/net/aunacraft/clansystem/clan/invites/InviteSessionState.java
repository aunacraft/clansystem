package net.aunacraft.clansystem.clan.invites;

public enum InviteSessionState {

    STARTING, WAITING, DENIED, ACCEPTED, CANCELLED

}
