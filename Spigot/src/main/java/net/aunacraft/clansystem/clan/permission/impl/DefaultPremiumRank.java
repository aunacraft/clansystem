package net.aunacraft.clansystem.clan.permission.impl;

import net.aunacraft.clansystem.clan.permission.ClanRank;
import net.aunacraft.clansystem.clan.permission.provider.PermissionProvider;
import org.bukkit.ChatColor;

public class DefaultPremiumRank extends ClanRank {
    public DefaultPremiumRank() {
        super(new PermissionProvider(), 2, "Premium", "Premium", ChatColor.YELLOW.toString());
        this.getPermissionProvider().insertDefaults();
    }
}
