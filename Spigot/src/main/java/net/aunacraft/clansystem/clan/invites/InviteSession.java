package net.aunacraft.clansystem.clan.invites;

import lombok.Getter;
import lombok.Setter;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.clansystem.clan.ClanPlayer;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.api.message.PluginMessage;
import net.aunacraft.cloud.api.message.PluginMessageListener;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.CopyOnWriteArrayList;

@Getter
@Setter
public class InviteSession {

    private static CopyOnWriteArrayList<InviteSession> sessions;
    private final ClanPlayer maintainer;
    private final String playerToInvite;
    private InviteSessionState currentState;

    /**
     * Returns an instance of an created Session
     *
     * @param maintainer
     * @param nameOfPlayerToInvite
     */
    public InviteSession(ClanPlayer maintainer, String nameOfPlayerToInvite) {
        if (sessions == null) sessions = new CopyOnWriteArrayList<>();
        this.maintainer = maintainer;
        this.playerToInvite = nameOfPlayerToInvite;
    }

    /**
     * Starts the Session
     */
    public void start() {
        this.currentState = InviteSessionState.STARTING; // Sets state
        /*
            Plugin message sending
         */
        PluginMessage pluginMessage = new PluginMessage("clansystem:inviteSession");
        pluginMessage.set("sessionstate", this.currentState.toString());
        pluginMessage.set("sessionowner", this.maintainer.toString());
        pluginMessage.set("invitedPlayer", this.playerToInvite);
        AunaCloudAPI.getMessageAPI().sendPluginMessage(AunaCloudAPI.getServiceAPI().getCurrentProxyService().getName(), pluginMessage);
        this.maintainer.toAunaPlayer().toBukkitPlayer().sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(this.maintainer.toBukkitPlayer(), "clan.invitesession.maintainer.startedmessage"));
        /*
        Sets PluginMessage receive listener
         */
        PluginMessageListener pluginMessageListener = null;
        PluginMessageListener finalPluginMessageListener = pluginMessageListener;
        pluginMessageListener = providedPluginMessage -> {
            if (pluginMessage.getType().equalsIgnoreCase("clansystem:inviteRequest")) {
                String s = pluginMessage.getString("invitedPlayer");
                if (s.equalsIgnoreCase(this.playerToInvite)) {
                    InviteSessionState state = InviteSessionState.valueOf(pluginMessage.getString("sessionstate"));
                    if (state == InviteSessionState.ACCEPTED)
                        new ClanPlayer(AunaAPI.getApi().getPlayer(playerToInvite).getUuid().toString(), ClanPlayer::updateSuffix);
                    else {
                        this.maintainer.toAunaPlayer().toBukkitPlayer().sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(maintainer.toBukkitPlayer(), "clan.invitesession.notaccepted"));
                    }
                    AunaCloudAPI.getMessageAPI().unregisterListener(finalPluginMessageListener);
                }
            }
        };
        AunaCloudAPI.getMessageAPI().registerListener(pluginMessageListener);
    }

    @Deprecated
    @NotNull
    public boolean isOnLocalService(String nameOfPlayer) {
        Bukkit.getOnlinePlayers().forEach(player -> {

        });
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getName().equals(nameOfPlayer))
                return true;
        }
        return false;

    }

}
