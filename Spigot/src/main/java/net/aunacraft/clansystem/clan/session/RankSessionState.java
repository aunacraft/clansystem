package net.aunacraft.clansystem.clan.session;

public enum RankSessionState {
    PAUSED, STARTED, NAMED, PERMISSED, COLORED, FINISHED, INTERRUPTED
}
