package net.aunacraft.clansystem.clan.permission.impl;

import net.aunacraft.clansystem.clan.permission.ClanRank;
import net.aunacraft.clansystem.clan.permission.provider.ClanPermission;
import net.aunacraft.clansystem.clan.permission.provider.PermissionProvider;
import org.bukkit.ChatColor;

public class DefaultModRank extends ClanRank {
    public DefaultModRank() {
        super(new PermissionProvider(), ClanRank.MAX_RANK_LEVEL - 1, "Mod", "Moderator", ChatColor.BLUE.toString());
        this.getPermissionProvider().insertDefaults();
        this.getPermissionProvider().setPermission(ClanPermission.KICK, true);
        this.getPermissionProvider().setPermission(ClanPermission.BAN, true);

    }
}
