package net.aunacraft.clansystem.clan.fonts;

import java.util.Locale;

public class FontReader {

    public String translate(final String text, Fonts fontType) {
        if (fontType.equals(Fonts.STD)) {
            return text.toUpperCase(Locale.ROOT);
        } else if (fontType.equals(Fonts.FANCY1)) {
            return text.toUpperCase(Locale.ROOT).replaceAll("G", "Ｇ")
                    .replaceAll("A", "Ａ")
                    .replaceAll("B", "Ｂ")
                    .replaceAll("C", "Ｃ")
                    .replaceAll("D", "Ｄ")
                    .replaceAll("E", "Ｅ")
                    .replaceAll("F", "Ｆ")
                    .replaceAll("H", "Ｈ")
                    .replaceAll("I", "Ｉ")
                    .replaceAll("J", "Ｊ")
                    .replaceAll("K", "Ｋ")
                    .replaceAll("L", "Ｌ")
                    .replaceAll("M", "Ｍ")
                    .replaceAll("N", "Ｎ")
                    .replaceAll("O", "Ｏ")
                    .replaceAll("P", "Ｐ")
                    .replaceAll("Q", "Ｑ")
                    .replaceAll("R", "Ｒ")
                    .replaceAll("S", "Ｓ")
                    .replaceAll("T", "Ｔ")
                    .replaceAll("U", "Ｕ")
                    .replaceAll("V", "Ｖ")
                    .replaceAll("W", "Ｗ")
                    .replaceAll("X", "Ｘ")
                    .replaceAll("Y", "Ｙ")
                    .replaceAll("Z", "ℤ");
        } else if (fontType.equals(Fonts.LITTLE)) {
            return text.toUpperCase(Locale.ROOT).replaceAll("G", "ɢ")
                    .replaceAll("A", "ᴀ")
                    .replaceAll("B", "ʙ")
                    .replaceAll("C", "ᴄ")
                    .replaceAll("D", "ᴅ")
                    .replaceAll("E", "ᴇ")
                    .replaceAll("F", "ꜰ")
                    .replaceAll("H", "ʜ")
                    .replaceAll("I", "ɪ")
                    .replaceAll("J", "ᴊ")
                    .replaceAll("K", "ᴋ")
                    .replaceAll("L", "ʟ")
                    .replaceAll("M", "ᴍ")
                    .replaceAll("N", "ɴ")
                    .replaceAll("O", "ᴏ")
                    .replaceAll("P", "ᴘ")
                    .replaceAll("Q", "Q")
                    .replaceAll("R", "ʀ")
                    .replaceAll("S", "ꜱ")
                    .replaceAll("T", "ᴛ")
                    .replaceAll("U", "ᴜ")
                    .replaceAll("V", "ᴠ")
                    .replaceAll("W", "ᴡ")
                    .replaceAll("X", "x")
                    .replaceAll("Y", "ʏ")
                    .replaceAll("Z", "ᴢ");
        } else {
            return text.toUpperCase(Locale.ROOT);
        }
    }

}
