package net.aunacraft.clansystem.clan.session;

import net.aunacraft.api.bukkit.signmenu.SignMenuFactory;
import net.aunacraft.api.util.validator.AunaValidators;
import net.aunacraft.clansystem.ClanSystem;
import net.aunacraft.clansystem.clan.Clan;
import net.aunacraft.clansystem.clan.ClanPlayer;
import net.aunacraft.clansystem.clan.permission.ClanRank;
import net.aunacraft.clansystem.clan.permission.provider.ClanPermission;
import net.aunacraft.clansystem.clan.permission.provider.PermissionProvider;
import net.aunacraft.clansystem.utils.ColorValidator;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

public class RankAddSession implements Listener {

    private final ClanPlayer player;
    private final Clan clan;
    private RankSessionState currentState;

    /**
     * Initializes the new Session
     *
     * @param player
     * @param clan
     */
    public RankAddSession(ClanPlayer player, Clan clan) {
        this.player = player;
        this.currentState = RankSessionState.PAUSED;
        this.clan = clan;
    }

    /**
     * Starts the new Session
     *
     * @param call
     */
    public void start(Consumer<ClanRank> call) {
        this.currentState = RankSessionState.STARTED; // Sets State
        if (this.player.hasPermission(ClanPermission.EDIT_RANKS, this.clan)) {
            // Creating new Thread
            Bukkit.getPluginManager().registerEvents(this, ClanSystem.getInstance());
            AtomicReference<String> name = new AtomicReference<>("");
            AtomicInteger rankLevel = new AtomicInteger(1);
            AtomicReference<String> color = new AtomicReference<>("");
            // Open Menu-Signs
            SignMenuFactory.newMenu("", "^^^^^^^^^^^^^^^", "Enter Rank-Name", "").response((sender, lines) -> {
                name.set(lines[0]);
                SignMenuFactory.newMenu("", "^^^^^^^^^^^^^^^", "Enter Rank-Level", "").response((sender1, lines1) -> {
                    if (AunaValidators.INT_VALIDATOR.isValid(lines1[0])) {
                        if (Integer.parseInt(lines1[0]) > 7 || Integer.parseInt(lines1[0]) < 1) {
                            this.currentState = RankSessionState.INTERRUPTED;
                        }
                        rankLevel.set(Integer.parseInt(lines1[0]));
                        SignMenuFactory.newMenu("", "^^^^^^^^^^^^^^^", "Colors: §aa§8,§bb§8,§cc§8,§dd§8,§ee§8,§ff", "§11§8,§22§8,§33§8,§44§8,§55§8,§66§8,§77§8,§88§8,§99").response((sender2, lines2) -> {
                            if (new ColorValidator().isValid(lines2[0])) {
                                color.set(lines2[0]);
                            } else {
                                this.currentState = RankSessionState.INTERRUPTED;
                            }
                            return false;
                        }).open(this.player.toAunaPlayer().toBukkitPlayer());
                    }
                    return false;
                }).open(this.player.toAunaPlayer().toBukkitPlayer());
                return false;
            }).open(this.player.toAunaPlayer().toBukkitPlayer());
            ClanRank rank = null;
            // Creating Rank
            if (!this.currentState.equals(RankSessionState.INTERRUPTED))
                rank = new ClanRank(new PermissionProvider(), rankLevel.get(), name.get().toLowerCase(Locale.ROOT), name.get(), ChatColor.getByChar(String.valueOf(color)).toString());
            else {
                this.player.toAunaPlayer().toBukkitPlayer().sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(this.player.toAunaPlayer().toBukkitPlayer(), "clan.rankaddsession.notvalid", ""));
                call.accept(null);
                return;
            }
            this.clan.addRank(rank);
            call.accept(rank);
            this.clan.pushChange();
        }

    }

    public void stop() {
        HandlerList.unregisterAll(this);
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        if (this.player.equals(event.getPlayer())) {
            this.player.toAunaPlayer().sendActionbar("§cΧΧΧΧΧΧΧΧΧΧΧΧΧΧΧΧΧΧΧΧΧ");
            event.setCancelled(true);
        }
    }

}
