package net.aunacraft.clansystem.clan.permission.provider;

import java.util.HashMap;

public class PermissionProvider {

    private final HashMap<ClanPermission, Boolean> permissionMap;

    public PermissionProvider() {
        this.permissionMap = new HashMap<>();
    }

    /**
     * Inserts the default Permissions (all false)
     */
    public void insertDefaults() {
        for (ClanPermission values : ClanPermission.values()) {
            permissionMap.put(values, false);
        }
    }

    public boolean hasPermission(final ClanPermission permission) {
        return this.permissionMap.get(permission);
    }

    public void setPermission(final ClanPermission key, final Boolean content) {
        permissionMap.replace(key, content);
    }

}
