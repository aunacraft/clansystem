package net.aunacraft.clansystem.clan.permission.impl;

import net.aunacraft.clansystem.clan.permission.ClanRank;
import net.aunacraft.clansystem.clan.permission.provider.PermissionProvider;
import org.bukkit.ChatColor;

public class DefaultMemberRank extends ClanRank {
    public DefaultMemberRank() {
        super(new PermissionProvider(), 1, "member", "Member", ChatColor.GRAY.toString());
        this.getPermissionProvider().insertDefaults();
    }
}
