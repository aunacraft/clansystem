package net.aunacraft.clansystem.listeners;

import net.aunacraft.api.event.impl.PlayerRegisterEvent;
import net.aunacraft.clansystem.clan.ClanPlayer;
import net.aunacraft.clansystem.clan.mysql.SQLManagement;

public class RegisterListener {

    public static void onRegister(final PlayerRegisterEvent event) {
        final String uuid = event.getPlayer().getUuid().toString();
        SQLManagement.existsClanPlayer(uuid, exists -> {
            if (exists) new ClanPlayer(uuid, clanPlayerConsumer -> clanPlayerConsumer.updateSuffix());
        });
    }

}