package net.aunacraft.clansystem.listeners;

import net.aunacraft.clansystem.clan.Clan;
import net.aunacraft.clansystem.clan.ClanPlayer;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.api.message.PluginMessage;

public class PluginMessageListener {

    public PluginMessageListener() {
        AunaCloudAPI.getMessageAPI().registerListener(this::handlePluginMessageReceive);
    }

    public void handlePluginMessageReceive(PluginMessage pluginMessage) {
        if (pluginMessage.getType().startsWith("clansystem:")) {
            if (pluginMessage.getType().equalsIgnoreCase("clansystem:tagupdate")) {
                Clan clan = Clan.fromString(pluginMessage.getString("clan"));
                clan.getPlayersAsync(clanPlayers -> {
                    for (ClanPlayer player : clanPlayers) player.updateSuffix();
                });
            }
        }
    }
}
