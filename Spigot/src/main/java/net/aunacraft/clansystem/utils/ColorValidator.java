package net.aunacraft.clansystem.utils;

import net.aunacraft.api.util.validator.AunaValidator;

public class ColorValidator implements AunaValidator {
    @Override
    public boolean isValid(String s) {
        for (char c : s.toCharArray()) {
            String string = String.valueOf(c);
            if (!string.matches("[a-fA-F0-9]")) {
                return false;
            }
        }
        if (s.length() == 1) {
            return true;
        } else
            return false;
    }
}
