package net.aunacraft.clansystem.utils;

import net.aunacraft.clansystem.clan.Clan;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.api.message.PluginMessage;

public class MessagingUtil {

    public static void sendGlobalClanMessage(String string, Clan clan) {
        PluginMessage message = new PluginMessage("clansystem:broadcast");
        message.set("clan", clan.toString());
        message.set("message", string);
        AunaCloudAPI.getMessageAPI().sendPluginMessage(AunaCloudAPI.getServiceAPI().getCurrentProxyService().getName(), message);
    }

}
