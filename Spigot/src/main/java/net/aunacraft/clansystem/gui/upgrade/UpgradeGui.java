package net.aunacraft.clansystem.gui.upgrade;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.clansystem.ClanSystem;
import net.aunacraft.clansystem.clan.Clan;
import net.aunacraft.clansystem.gui.settings.SettingsIndexGui;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

public class UpgradeGui extends Gui {

    private final Clan clan;
    private final AunaPlayer player;

    public UpgradeGui(final Clan clan, final AunaPlayer player) {
        super(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.gui.settings.upgrade.title"), 36);
        this.clan = clan;
        this.player = player;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        guiInventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(11, new ItemBuilder(Material.GREEN_BANNER).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.upgrade.upgradedp"))
                .setLore(ClanSystem.getInstance().getMessageService()
                        .getMessageForPlayer(player, "clan.gui.settings.upgrade.upgrade.lore1", (clan.getLevel() == 1 ? "10k" : clan.getLevel() == 2 ? "50k" : clan.getLevel() == 3 ? "250k" : clan.getLevel() == 4 ? "500k" :
                                ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.upgrade.lore.alreadyhighestlevel")))).build());
        guiInventory.setItem(15, new ItemBuilder(Material.GOLD_BLOCK).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.upgrade.infodp", clan.getLevel())).build());
        guiInventory.setItem(27, new ItemBuilder(Material.ARROW).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.back")).build());
    }

    @Override
    public void onClick(Player player, InventoryClickEvent inventoryClickEvent) {
        if (inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.back"))) {
            new SettingsIndexGui(this.player, this.clan).open(player);
        } else if (inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.upgrade.upgradedp"))) {
            if (clan.getLevel() == 1) {
                if (this.player.getCoins() >= 10000) {
                    clan.setLevel(2);
                    clan.pushChange();
                    AunaAPI.getApi().removeCoins(this.player.getUuid(), 10000);
                    player.closeInventory();
                    player.sendTitle(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.clanleveluptitle1"),
                            ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.clanleveluptitle2", clan.getLevel()));
                } else {
                    player.sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.tolowmoneylevel"));
                }
            } else if (clan.getLevel() == 2) {
                if (this.player.getCoins() >= 50000) {
                    clan.setLevel(3);
                    clan.pushChange();
                    AunaAPI.getApi().removeCoins(this.player.getUuid(), 50000);
                    player.closeInventory();
                    player.sendTitle(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.clanleveluptitle1"),
                            ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.clanleveluptitle2", clan.getLevel()));
                } else {
                    player.sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.tolowmoneylevel"));
                }
            } else if (clan.getLevel() == 3) {
                if (this.player.getCoins() >= 250000) {
                    clan.setLevel(4);
                    clan.pushChange();
                    AunaAPI.getApi().removeCoins(this.player.getUuid(), 250000);
                    player.closeInventory();
                    player.sendTitle(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.clanleveluptitle1"),
                            ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.clanleveluptitle2", clan.getLevel()));
                } else {
                    player.sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.tolowmoneylevel"));
                }
            } else if (clan.getLevel() == 4) {
                if (this.player.getCoins() >= 500000) {
                    clan.setLevel(5);
                    clan.pushChange();
                    AunaAPI.getApi().removeCoins(this.player.getUuid(), 500000);
                    player.closeInventory();
                    player.sendTitle(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.clanleveluptitle1"),
                            ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.clanleveluptitle2", clan.getLevel()));
                } else {
                    player.sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.tolowmoneylevel"));
                }
            } else {
                player.sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.alreadyhighestlevel"));
            }
        }
    }
}
