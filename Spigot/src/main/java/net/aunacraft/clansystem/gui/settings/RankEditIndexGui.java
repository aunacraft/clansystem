package net.aunacraft.clansystem.gui.settings;

import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.api.util.SkullBuilder;
import net.aunacraft.clansystem.ClanSystem;
import net.aunacraft.clansystem.clan.Clan;
import net.aunacraft.clansystem.clan.ClanPlayer;
import net.aunacraft.clansystem.clan.permission.ClanRank;
import net.aunacraft.clansystem.clan.session.RankAddSession;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

public class RankEditIndexGui extends Gui {
    private final AunaPlayer player;
    private final Clan clan;

    public RankEditIndexGui(Clan clan, AunaPlayer player) {
        super(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.gui.rankeditindex.title"), 36);
        this.player = player;
        this.clan = clan;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        guiInventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(27, new ItemBuilder(Material.ARROW).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.back")).build());
        guiInventory.setItem(31, new ItemBuilder(SkullBuilder.getSkull("http://textures.minecraft.net/texture/b056bc1244fcff99344f12aba42ac23fee6ef6e3351d27d273c1572531f"))
                .setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.rankeditindex.additem")).build());
        guiInventory.setItem(33, new ItemBuilder(Material.NAME_TAG).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.rankeditindex.prioritysetitem")).build());
        for (int i = 9; i < 18; i++)
            guiInventory.setItem(i, new ItemBuilder(Material.RED_STAINED_GLASS_PANE).setDisplayName(" ").build());
        int slotCount = 9;
        for (ClanRank ranks : clan.getRanks()) {
            guiInventory.setItem(slotCount, new ItemBuilder((
                    clan.getColor().equalsIgnoreCase("§1") ? Material.BLUE_CONCRETE : clan.getColor().equalsIgnoreCase("§2") ? Material.GREEN_CONCRETE :
                            clan.getColor().equalsIgnoreCase("§3") ? Material.BLUE_CONCRETE : clan.getColor().equalsIgnoreCase("§4") ? Material.RED_CONCRETE :
                                    clan.getColor().equalsIgnoreCase("§5") ? Material.PURPLE_CONCRETE : clan.getColor().equalsIgnoreCase("§6") ? Material.ORANGE_CONCRETE :
                                            clan.getColor().equalsIgnoreCase("§7") ? Material.BLUE_CONCRETE : clan.getColor().equalsIgnoreCase("§8") ? Material.GRAY_CONCRETE :
                                                    clan.getColor().equalsIgnoreCase("§9") ? Material.BLUE_CONCRETE : clan.getColor().equalsIgnoreCase("§a") ? Material.GREEN_CONCRETE :
                                                            clan.getColor().equalsIgnoreCase("§b") ? Material.BLUE_CONCRETE : clan.getColor().equalsIgnoreCase("§c") ? Material.RED_CONCRETE :
                                                                    clan.getColor().equalsIgnoreCase("§d") ? Material.PURPLE_CONCRETE : clan.getColor().equalsIgnoreCase("§e") ? Material.YELLOW_CONCRETE :
                                                                            clan.getColor().equalsIgnoreCase("§f") ? Material.WHITE_CONCRETE : Material.BLUE_CONCRETE
            )).setDisplayName(ranks.getColor() + ranks.getDisplayName()).setLore(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.membersettings.rankitem.lore1", ranks.getRankLevel())).build());
            slotCount++;
        }
    }

    @Override
    public void onClick(Player player, InventoryClickEvent inventoryClickEvent) {
        if (inventoryClickEvent.getSlot() == 27) {
            new SettingsIndexGui(this.player, this.clan).open(player);
        } else if (inventoryClickEvent.getSlot() == 31) {
            player.closeInventory();
            new ClanPlayer(this.player.getUuid().toString(), cp -> new RankAddSession(cp, this.clan).start(rank -> {
                new RankEditIndexGui(cp.getClan(), this.player).open(player);
            }));
        } else if (inventoryClickEvent.getSlot() == 33) {

        }
    }
}
