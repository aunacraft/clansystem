package net.aunacraft.clansystem.gui.settings;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.clansystem.ClanSystem;
import net.aunacraft.clansystem.clan.Clan;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public class ColorSettingsGui extends Gui {

    private final Clan clan;

    public ColorSettingsGui(final AunaPlayer player, final Clan clan) {
        super(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.gui.settings.title"), 54);
        this.clan = clan;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        guiInventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName(" ").build());
        int[] airSlots = new int[]{28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};
        for (int slot : airSlots)
            guiInventory.setItem(slot, new ItemStack(Material.AIR));
        guiInventory.setItem(45, new ItemBuilder(Material.ARROW).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.back")).build());
        guiInventory.setItem(12, new ItemBuilder(Material.OAK_SIGN).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.category.state")).addItemFlags(ItemFlag.HIDE_ENCHANTS).build());
        guiInventory.setItem(14, new ItemBuilder(Material.BOOK).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.category.font")).addItemFlags(ItemFlag.HIDE_ENCHANTS).build());
        guiInventory.setItem(13, new ItemBuilder(Material.NAME_TAG).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.category.color")).addEnchant(Enchantment.DAMAGE_ALL, 1).addItemFlags(ItemFlag.HIDE_ENCHANTS).build());
        guiInventory.setItem(29, new ItemBuilder(Material.YELLOW_BANNER).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.color.select", "§e")).build());
        guiInventory.setItem(31, new ItemBuilder(Material.ORANGE_BANNER).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.color.select", "§6")).build());
        guiInventory.setItem(33, new ItemBuilder(Material.PURPLE_BANNER).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.color.select", "§5")).build());
        guiInventory.setItem(37, new ItemBuilder(Material.GREEN_BANNER).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.color.select", "§a")).build());
        guiInventory.setItem(39, new ItemBuilder(Material.BLUE_BANNER).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.color.select", "§1")).build());
        guiInventory.setItem(41, new ItemBuilder(Material.RED_BANNER).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.color.select", "§c")).build());
        guiInventory.setItem(43, new ItemBuilder(Material.BLACK_BANNER).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.color.select", "§0")).build());
    }

    @Override
    public void onClick(Player player, InventoryClickEvent inventoryClickEvent) {
        if (inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.category.state"))) {
            new StateSettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
        } else if (inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.category.color"))) {
            new ColorSettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
        } else if (inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.back"))) {
            new SettingsIndexGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
        } else if (inventoryClickEvent.getCurrentItem().getType().equals(Material.YELLOW_BANNER)) {
            if (this.clan.getLevel() >= 2) {
                this.clan.setColor("§e");
                this.clan.pushChange();
                new ColorSettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
            } else {
                ClanSystem.getInstance().getMessageService().sendMessage(player, "clan.tolowlevel", 2);
            }
        } else if (inventoryClickEvent.getCurrentItem().getType().equals(Material.ORANGE_BANNER)) {
            if (this.clan.getLevel() >= 3) {
                this.clan.setColor("§6");
                this.clan.pushChange();
                new ColorSettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
            } else {
                ClanSystem.getInstance().getMessageService().sendMessage(player, "clan.tolowlevel", 3);
            }
        } else if (inventoryClickEvent.getCurrentItem().getType().equals(Material.PURPLE_BANNER)) {
            if (this.clan.getLevel() >= 2) {
                this.clan.setColor("§5");
                this.clan.pushChange();
                new ColorSettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
            } else {
                ClanSystem.getInstance().getMessageService().sendMessage(player, "clan.tolowlevel", 2);
            }
        } else if (inventoryClickEvent.getCurrentItem().getType().equals(Material.GREEN_BANNER)) {
            if (this.clan.getLevel() >= 1) {
                this.clan.setColor("§a");
                this.clan.pushChange();
            } else {
                ClanSystem.getInstance().getMessageService().sendMessage(player, "clan.tolowlevel", 1);
            }
        } else if (inventoryClickEvent.getCurrentItem().getType().equals(Material.BLUE_BANNER)) {
            if (this.clan.getLevel() >= 2) {
                this.clan.setColor("§1");
                this.clan.pushChange();
                new ColorSettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
            } else {
                ClanSystem.getInstance().getMessageService().sendMessage(player, "clan.tolowlevel", 2);
            }
        } else if (inventoryClickEvent.getCurrentItem().getType().equals(Material.RED_BANNER)) {
            if (this.clan.getLevel() >= 3) {
                this.clan.setColor("§c");
                this.clan.pushChange();
                new ColorSettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
            } else {
                ClanSystem.getInstance().getMessageService().sendMessage(player, "clan.tolowlevel", 3);
            }
        } else if (inventoryClickEvent.getCurrentItem().getType().equals(Material.BLACK_BANNER)) {
            if (this.clan.getLevel() >= 2) {
                this.clan.setColor("§0");
                this.clan.pushChange();
                new ColorSettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
            } else {
                ClanSystem.getInstance().getMessageService().sendMessage(player, "clan.tolowlevel", 2);
            }
        } else if (inventoryClickEvent.getSlot() == 14) {
            new FontSettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
        }
    }
}
