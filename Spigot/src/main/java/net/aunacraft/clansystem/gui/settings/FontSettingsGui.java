package net.aunacraft.clansystem.gui.settings;

import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.clansystem.ClanSystem;
import net.aunacraft.clansystem.clan.Clan;
import net.aunacraft.clansystem.clan.fonts.Fonts;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public class FontSettingsGui extends Gui {

    private final AunaPlayer player;
    private final Clan clan;

    public FontSettingsGui(final AunaPlayer player, final Clan clan) {
        super(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.gui.settings.title"), 54);
        this.player = player;
        this.clan = clan;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        guiInventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName(" ").build());
        int[] airSlots = new int[]{28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};
        for (int slot : airSlots)
            guiInventory.setItem(slot, new ItemStack(Material.AIR));
        guiInventory.setItem(12, new ItemBuilder(Material.OAK_SIGN).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.category.state")).build());
        guiInventory.setItem(13, new ItemBuilder(Material.NAME_TAG).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.category.color")).build());
        guiInventory.setItem(14, new ItemBuilder(Material.BOOK).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.category.font")).addEnchant(Enchantment.DAMAGE_ALL, 1).addItemFlags(ItemFlag.HIDE_ENCHANTS).build());
        guiInventory.setItem(45, new ItemBuilder(Material.ARROW).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.back")).build());
        ItemStack onlyInvite = new ItemBuilder(Material.GOLD_INGOT).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.font.std")).build();
        if (this.clan.getFont().equals(Fonts.STD))
            onlyInvite = new ItemBuilder(onlyInvite).addEnchant(Enchantment.DURABILITY, 1).addItemFlags(ItemFlag.HIDE_ENCHANTS).build();
        ItemStack opend = new ItemBuilder(Material.DIAMOND).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.font.fancy")).build();
        if (this.clan.getFont().equals(Fonts.FANCY1))
            opend = new ItemBuilder(opend).addEnchant(Enchantment.DURABILITY, 1).addItemFlags(ItemFlag.HIDE_ENCHANTS).build();
        ItemStack closed = new ItemBuilder(Material.EMERALD).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.font.little")).build();
        if (this.clan.getFont().equals(Fonts.LITTLE))
            closed = new ItemBuilder(closed).addEnchant(Enchantment.DURABILITY, 1).addItemFlags(ItemFlag.HIDE_ENCHANTS).build();
        guiInventory.setItem(31, onlyInvite);
        guiInventory.setItem(38, opend);
        guiInventory.setItem(42, closed);
    }

    @Override
    public void onClick(Player player, InventoryClickEvent inventoryClickEvent) {
        if (inventoryClickEvent.getSlot() == 45) {
            new SettingsIndexGui(this.player, this.clan).open(player);
        } else if (inventoryClickEvent.getSlot() == 12) {
            new StateSettingsGui(this.player, this.clan).open(player);
        } else if (inventoryClickEvent.getSlot() == 13) {
            new ColorSettingsGui(this.player, this.clan).open(player);
        } else if (inventoryClickEvent.getSlot() == 31) {
            this.clan.setFont(Fonts.STD);
            this.clan.pushChange();
            new FontSettingsGui(this.player, this.clan).open(player);
        } else if (inventoryClickEvent.getSlot() == 38) {
            if (this.clan.getLevel() > 1) {
                this.clan.setFont(Fonts.FANCY1);
                this.clan.pushChange();
                new FontSettingsGui(this.player, this.clan).open(player);
            } else {
                ClanSystem.getInstance().getMessageService().sendMessage(player, "clan.tolowlevel", 2);
            }
        } else if (inventoryClickEvent.getSlot() == 42) {
            if (this.clan.getLevel() > 2) {
                this.clan.setFont(Fonts.LITTLE);
                this.clan.pushChange();
                new FontSettingsGui(this.player, this.clan).open(player);
            } else {
                ClanSystem.getInstance().getMessageService().sendMessage(player, "clan.tolowlevel", 3);
            }
        }
    }
}
