package net.aunacraft.clansystem.gui.settings;

import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.clansystem.ClanSystem;
import net.aunacraft.clansystem.clan.Clan;
import net.aunacraft.clansystem.clan.ClanPlayer;
import net.aunacraft.clansystem.clan.permission.ClanRank;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class MemberManageGui extends Gui {

    private final ClanPlayer playerToManage;
    private final AunaPlayer opener;
    private final Clan clan;

    public MemberManageGui(final ClanPlayer playerToManage, final AunaPlayer player, final Clan clan) {
        super(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.gui.settings.membermanage.title", playerToManage.toAunaPlayer().getName()), 45);
        this.playerToManage = playerToManage;
        this.opener = player;
        this.clan = clan;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        guiInventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(36, new ItemBuilder(Material.ARROW).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.back")).build());
        guiInventory.setItem(13, new ItemBuilder(Material.NETHERITE_BOOTS).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.membersettings.kick")).build());
        int nextRankSlot = 27;
        for (ClanRank ranks : clan.getRanks()) {
            guiInventory.setItem(nextRankSlot, new ItemBuilder((
                    clan.getColor().equalsIgnoreCase("§1") ? Material.BLUE_CONCRETE : clan.getColor().equalsIgnoreCase("§2") ? Material.GREEN_CONCRETE :
                            clan.getColor().equalsIgnoreCase("§3") ? Material.BLUE_CONCRETE : clan.getColor().equalsIgnoreCase("§4") ? Material.RED_CONCRETE :
                                    clan.getColor().equalsIgnoreCase("§5") ? Material.PURPLE_CONCRETE : clan.getColor().equalsIgnoreCase("§6") ? Material.ORANGE_CONCRETE :
                                            clan.getColor().equalsIgnoreCase("§7") ? Material.BLUE_CONCRETE : clan.getColor().equalsIgnoreCase("§8") ? Material.GRAY_CONCRETE :
                                                    clan.getColor().equalsIgnoreCase("§9") ? Material.BLUE_CONCRETE : clan.getColor().equalsIgnoreCase("§a") ? Material.GREEN_CONCRETE :
                                                            clan.getColor().equalsIgnoreCase("§b") ? Material.BLUE_CONCRETE : clan.getColor().equalsIgnoreCase("§c") ? Material.RED_CONCRETE :
                                                                    clan.getColor().equalsIgnoreCase("§d") ? Material.PURPLE_CONCRETE : clan.getColor().equalsIgnoreCase("§e") ? Material.YELLOW_CONCRETE :
                                                                            clan.getColor().equalsIgnoreCase("§f") ? Material.WHITE_CONCRETE : Material.BLUE_CONCRETE
            )).setDisplayName(ranks.getColor() + ranks.getDisplayName()).setLore(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.membersettings.rankitem.lore1", ranks.getRankLevel())).build());
            nextRankSlot++;
        }
        for (int i = nextRankSlot; i < 45; i++) guiInventory.setItem(i, new ItemStack(Material.AIR));
    }

    @Override
    public void onClick(Player player, InventoryClickEvent inventoryClickEvent) {
        if (inventoryClickEvent.getSlot() == 36) {
            new UserManagementIndex(this.opener, this.clan).open(player);
        } else if (inventoryClickEvent.getSlot() == 13) {
            this.playerToManage.delete();
            player.closeInventory();
            player.sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.membersettings.kick.message", this.playerToManage.toAunaPlayer().getName()));
        } else if (inventoryClickEvent.getCurrentItem().getType().equals(Material.BLACK_STAINED_GLASS_PANE)) ;
        else {
            clan.getRanks().forEach(rank -> {
                if (rank.getDisplayName().equalsIgnoreCase(inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName())) {
                    playerToManage.setRankName(rank.getGroupName());
                    playerToManage.pushChange();
                    player.sendMessage(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.membersettings.setrankmessage", this.playerToManage.toAunaPlayer().getName(), rank.getDisplayName()));
                }
            });
        }
    }
}
