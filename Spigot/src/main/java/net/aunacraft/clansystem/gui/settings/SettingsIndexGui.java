package net.aunacraft.clansystem.gui.settings;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.api.util.SkullBuilder;
import net.aunacraft.clansystem.ClanSystem;
import net.aunacraft.clansystem.clan.Clan;
import net.aunacraft.clansystem.gui.upgrade.UpgradeGui;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

public class SettingsIndexGui extends Gui {

    private final AunaPlayer player;
    private final Clan clan;

    public SettingsIndexGui(final AunaPlayer player, final Clan clan) {
        super(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.gui.settingsindex.title"), 27);
        this.player = player;
        this.clan = clan;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        guiInventory.fill(new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(10, new ItemBuilder(SkullBuilder.getSkull("http://textures.minecraft.net/texture/46ba63344f49dd1c4f5488e926bf3d9e2b29916a6c50d610bb40a5273dc8c82"))
                .setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settingsindex.settingsdp")).build());
        guiInventory.setItem(12, new ItemBuilder(SkullBuilder.getSkull("http://textures.minecraft.net/texture/45587da7fe7336e8ab9f791ea5e2cfc8a827ca959567eb9d53a647babf948d5"))
                .setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settingsindex.rankdp")).build());
        guiInventory.setItem(14, new ItemBuilder(SkullBuilder.getSkull("http://textures.minecraft.net/texture/46ba63344f49dd1c4f5488e926bf3d9e2b29916a6c50d610bb40a5273dc8c82"))
                .setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settingsindex.userdp")).build());
        guiInventory.setItem(16, new ItemBuilder(SkullBuilder.getSkull("http://textures.minecraft.net/texture/46ba63344f49dd1c4f5488e926bf3d9e2b29916a6c50d610bb40a5273dc8c82"))
                .setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settingsindex.generaldp")).build());
    }

    @Override
    public void onClick(Player player, InventoryClickEvent inventoryClickEvent) {
        if (inventoryClickEvent.getClickedInventory() == null) return;
        if (inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settingsindex.settingsdp"))) {
            new StateSettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
        } else if (inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settingsindex.generaldp"))) {
            new UpgradeGui(this.clan, this.player).open(player);
        } else if (inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settingsindex.userdp"))) {
            new UserManagementIndex(this.player, this.clan).open(player);
        }
        switch (inventoryClickEvent.getSlot()) {
            case 14:
                new UserManagementIndex(this.player, this.clan).open(player);
                break;
            case 12:
                new RankEditIndexGui(this.clan, this.player).open(player);
                break;
            default:
                break;
        }
    }
}
