package net.aunacraft.clansystem.gui.settings;

import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.clansystem.ClanSystem;
import net.aunacraft.clansystem.clan.Clan;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.concurrent.atomic.AtomicInteger;

public class RankPriorityGuiSession extends Gui {

    private final Clan clan;
    private final AunaPlayer player;

    public RankPriorityGuiSession(final AunaPlayer player, final Clan clan) {
        super(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.gui.rankeditpriority.title"), 18);
        this.player = player;
        this.clan = clan;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        for (int i = 10; i < 18; ++i)
            guiInventory.setItem(i, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(9, new ItemBuilder(Material.ARROW).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.back")).build());
        final AtomicInteger slotCount = new AtomicInteger(0);
        this.clan.getRanks().forEach(ranks -> {
            guiInventory.setItem(slotCount.get(), new ItemBuilder((
                    clan.getColor().equalsIgnoreCase("§1") ? Material.BLUE_CONCRETE : clan.getColor().equalsIgnoreCase("§2") ? Material.GREEN_CONCRETE :
                            clan.getColor().equalsIgnoreCase("§3") ? Material.BLUE_CONCRETE : clan.getColor().equalsIgnoreCase("§4") ? Material.RED_CONCRETE :
                                    clan.getColor().equalsIgnoreCase("§5") ? Material.PURPLE_CONCRETE : clan.getColor().equalsIgnoreCase("§6") ? Material.ORANGE_CONCRETE :
                                            clan.getColor().equalsIgnoreCase("§7") ? Material.BLUE_CONCRETE : clan.getColor().equalsIgnoreCase("§8") ? Material.GRAY_CONCRETE :
                                                    clan.getColor().equalsIgnoreCase("§9") ? Material.BLUE_CONCRETE : clan.getColor().equalsIgnoreCase("§a") ? Material.GREEN_CONCRETE :
                                                            clan.getColor().equalsIgnoreCase("§b") ? Material.BLUE_CONCRETE : clan.getColor().equalsIgnoreCase("§c") ? Material.RED_CONCRETE :
                                                                    clan.getColor().equalsIgnoreCase("§d") ? Material.PURPLE_CONCRETE : clan.getColor().equalsIgnoreCase("§e") ? Material.YELLOW_CONCRETE :
                                                                            clan.getColor().equalsIgnoreCase("§f") ? Material.WHITE_CONCRETE : Material.BLUE_CONCRETE
            )).setDisplayName(ranks.getColor() + ranks.getDisplayName()).setLore(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.membersettings.rankitem.lore1", ranks.getRankLevel())).build());
            slotCount.getAndIncrement();
        });
    }

    @Override
    public void onClick(Player player, InventoryClickEvent inventoryClickEvent) {
        if (inventoryClickEvent.getSlot() == 9) {
            new RankEditIndexGui(this.clan, this.player).open(player);
        }
    }
}
