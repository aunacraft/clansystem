package net.aunacraft.clansystem.gui.settings;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.api.util.SkullBuilder;
import net.aunacraft.clansystem.ClanSystem;
import net.aunacraft.clansystem.clan.Clan;
import net.aunacraft.clansystem.clan.ClanPlayer;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.List;
import java.util.UUID;

public class UserManagementIndex extends Gui {

    private final Clan clan;
    private final AunaPlayer player;
    private int currentSite;
    private int maxSites;
    private List<ClanPlayer> clanPlayers;

    public UserManagementIndex(final AunaPlayer player, final Clan clan) {
        super(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.gui.settings.memberindex.title"), 9 * 6);
        this.clan = clan;
        this.player = player;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        this.clan.getPlayersAsync(array -> {
            this.clanPlayers = array;
            this.maxSites = array.size() / 9 * 6 - 8;
            this.currentSite = 1;
            if (maxSites < 1) maxSites = 1;
            guiInventory.setItem(9 * 5, new ItemBuilder(Material.ARROW).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.back")).build());
            for (int i = 0; i < 9 * 6 - 9; i++) {
                guiInventory.setItem(i, new ItemBuilder(SkullBuilder.getSkull(((CraftPlayer) this.clanPlayers.get(this.currentSite == 1 ? i : (i + ((9 * 6 - 9) * this.maxSites))).toAunaPlayer().toBukkitPlayer()).getProfile()))
                        .setDisplayName(AunaAPI.getApi().getPlayer(UUID.fromString(this.clanPlayers.get(this.currentSite == 1 ? i : (i + ((9 * 6 - 9) * this.maxSites))).getUuid())).getName())
                        .setLore(/*
                        ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.memberindex.lore",
                                this.clan.getRankFromName(
                                        this.clanPlayers.get(this.currentSite == 1 ? i : (i + ((9 * 6 - 9) * this.maxSites)))
                                                .getRankName())
                                        .getColor() +
                                        this.clan.getRankFromName(this.clanPlayers.get(this.currentSite == 1 ? i : (i + ((9 * 6 - 9) * this.maxSites))).getRankName()).getDisplayName())*/"NIX :(")
                        .build());
            }
        });
    }

    @Override
    public void onClick(Player player, InventoryClickEvent inventoryClickEvent) {
        if (inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.back"))) {
            new SettingsIndexGui(this.player, this.clan).open(player);
        } else {
            String ingameName = inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName();
            AunaPlayer clickedPlayer = AunaAPI.getApi().getPlayer(ingameName);
            ClanPlayer clickedClanPlayer = new ClanPlayer(clickedPlayer.getUuid().toString(), clanPlayerCallback -> {
                new MemberManageGui(clanPlayerCallback, this.player, this.clan).open(player);
            });
        }
    }
}