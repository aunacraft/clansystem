package net.aunacraft.clansystem.gui.settings;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.clansystem.ClanSystem;
import net.aunacraft.clansystem.clan.Clan;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public class StateSettingsGui extends Gui {

    private final Clan clan;

    public StateSettingsGui(final AunaPlayer player, final Clan clan) {
        super(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "clan.gui.settings.title"), 54);
        this.clan = clan;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        guiInventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName(" ").build());
        int[] airSlots = new int[]{28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};
        for (int slot : airSlots)
            guiInventory.setItem(slot, new ItemStack(Material.AIR));
        guiInventory.setItem(12, new ItemBuilder(Material.OAK_SIGN).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.category.state")).addEnchant(Enchantment.DAMAGE_ALL, 1).addItemFlags(ItemFlag.HIDE_ENCHANTS).build());
        guiInventory.setItem(13, new ItemBuilder(Material.NAME_TAG).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.category.color")).build());
        guiInventory.setItem(14, new ItemBuilder(Material.BOOK).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.category.font")).addItemFlags(ItemFlag.HIDE_ENCHANTS).build());
        guiInventory.setItem(45, new ItemBuilder(Material.ARROW).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.back")).build());
        ItemStack onlyInvite = new ItemBuilder(Material.YELLOW_TERRACOTTA).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.state.onlyinvite")).build();
        if (this.clan.getClanState().equals(Clan.ClanState.INVITE))
            onlyInvite = new ItemBuilder(onlyInvite).addEnchant(Enchantment.DURABILITY, 1).addItemFlags(ItemFlag.HIDE_ENCHANTS).build();
        ItemStack opend = new ItemBuilder(Material.GREEN_TERRACOTTA).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.state.opend")).build();
        if (this.clan.getClanState().equals(Clan.ClanState.OPEN))
            opend = new ItemBuilder(opend).addEnchant(Enchantment.DURABILITY, 1).addItemFlags(ItemFlag.HIDE_ENCHANTS).build();
        ItemStack closed = new ItemBuilder(Material.RED_TERRACOTTA).setDisplayName(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.state.closed")).build();
        if (this.clan.getClanState().equals(Clan.ClanState.CLOSE))
            closed = new ItemBuilder(closed).addEnchant(Enchantment.DURABILITY, 1).addItemFlags(ItemFlag.HIDE_ENCHANTS).build();
        guiInventory.setItem(31, onlyInvite);
        guiInventory.setItem(38, opend);
        guiInventory.setItem(42, closed);
    }

    @Override
    public void onClick(Player player, InventoryClickEvent inventoryClickEvent) {
        if (inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.category.state"))) {
            new StateSettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
        } else if (inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.category.color"))) {
            new ColorSettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
        } else if (inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.back"))) {
            new SettingsIndexGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
        } else if (inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.state.onlyinvite"))) {
            this.clan.setClanState(Clan.ClanState.INVITE);
            this.clan.pushChange();
            new StateSettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
        } else if (inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.state.opend"))) {
            this.clan.setClanState(Clan.ClanState.OPEN);
            this.clan.pushChange();
            new StateSettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
        } else if (inventoryClickEvent.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ClanSystem.getInstance().getMessageService().getMessageForPlayer(player, "clan.gui.settings.state.closed"))) {
            this.clan.setClanState(Clan.ClanState.CLOSE);
            this.clan.pushChange();
            new StateSettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
        } else if (inventoryClickEvent.getSlot() == 14) {
            new FontSettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId()), this.clan).open(player);
        }
    }

    @Override
    public void onClose(Player player) {
        super.onClose(player);
        this.clan.pushChange();
    }
}
