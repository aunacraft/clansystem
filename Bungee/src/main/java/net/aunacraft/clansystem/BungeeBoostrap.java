package net.aunacraft.clansystem;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.database.DatabaseHandler;
import net.aunacraft.api.database.config.impl.BungeeDatabaseConfig;
import net.aunacraft.clansystem.listeners.ChatListener;
import net.aunacraft.clansystem.listeners.PluginMessageListener;
import net.md_5.bungee.api.plugin.Plugin;

public class BungeeBoostrap extends Plugin {

    public static BungeeBoostrap instance;
    public DatabaseHandler dbHandler;

    @Override
    public void onEnable() {
        instance = this;
        this.getProxy().getPluginManager().registerListener(this, new ChatListener());
        dbHandler = new DatabaseHandler(new BungeeDatabaseConfig(this));
        AunaAPI.getApi().getMessageService().applyPrefix("clan", "clan.prefix");
        new PluginMessageListener();
    }

}
