package net.aunacraft.clansystem.clan.permission.provider;


public enum ClanPermission {

    CHANGE_STATE,
    DEPOSIT_BANK,
    WITHDRAW_BANK,
    EDIT_RANKS,
    KICK,
    BAN,
    DELETE
}
