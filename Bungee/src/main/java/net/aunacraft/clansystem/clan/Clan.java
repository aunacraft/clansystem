package net.aunacraft.clansystem.clan;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import lombok.Setter;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.clansystem.BungeeBoostrap;
import net.aunacraft.clansystem.clan.fonts.Fonts;
import net.aunacraft.clansystem.clan.mysql.SQLManagement;
import net.aunacraft.clansystem.clan.permission.ClanRank;
import net.aunacraft.clansystem.clan.permission.impl.DefaultMemberRank;
import net.aunacraft.clansystem.clan.permission.impl.DefaultModRank;
import net.aunacraft.clansystem.clan.permission.impl.DefaultOwnerRank;
import net.aunacraft.clansystem.clan.permission.impl.DefaultPremiumRank;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

@Getter
@Setter
public class Clan {

    public static final ClanState DEFAULT_STATE = ClanState.INVITE;
    public static final int DEFAULT_LEVEL = 1;
    public static final String DEFAULT_COLOR = "§a";
    private String name;
    private String tag;
    private String color;
    private Fonts font;
    private int level;
    private long clanBankAmount;
    private ClanState clanState;
    private List<ClanRank> ranks;

    /**
     * Gets the Class by the tag from mysql
     *
     * @param tag
     * @param call
     */
    public Clan(final String tag, final Consumer<Clan> call) {
        this.tag = tag;
        SQLManagement.getClanValuesByClanTag(this.tag, strings -> {
            name = strings[0];
            color = strings[1];
            level = Integer.parseInt(strings[2]);
            clanBankAmount = Long.parseLong(strings[3]);
            unserializeJsonList(strings[4]);
            clanState = ClanState.valueOf(strings[5]);
            this.font = Fonts.valueOf(strings[6]);
            if (call != null) {
                call.accept(this);
            }
        });
    }

    /**
     * Initializes the Class (Used for new Clans)
     *
     * @param name
     * @param tag
     * @param color
     * @param level
     */
    public Clan(final String name, final String tag, final String color, final int level) {
        this.name = name;
        this.tag = tag;
        this.color = color;
        this.level = level;
        this.clanState = Clan.DEFAULT_STATE;
        this.ranks = new ArrayList<>();
        this.clanBankAmount = 0;
        this.font = Fonts.STD;
    }

    public static Clan fromString(final String from) {
        return new Gson().fromJson(from, Clan.class);
    }

    public void addRank(final ClanRank rank) {
        if (!ranks.contains(rank)) ranks.add(rank);
    }

    public void removeRank(final ClanRank rank) {
        ranks.remove(rank);
    }

    /**
     * Returns all ClanPlayers
     *
     * @param callback
     */
    public void getPlayersAsync(final Consumer<List<ClanPlayer>> callback) {
        BungeeBoostrap.instance.dbHandler.createBuilder("SELECT * FROM clan_player WHERE tag=?").addObjects(this.tag).queryAsync(rs -> {
            try {
                final List<ClanPlayer> playerArray = Lists.newArrayList();
                while (rs.next()) playerArray.add(new ClanPlayer(this.tag, rs.getString("uuid"), rs.getString("rank")));
                callback.accept(playerArray);
            } catch (SQLException exception) {
            }
        });
    }

    /**
     * Gets a Rank from RankNAme
     *
     * @param name
     * @return ClanRank
     */
    public ClanRank getRankFromName(final String name) {
        for (ClanRank ranks : this.ranks) {
            if (ranks.getGroupName() == name)
                return ranks;
        }
        return null;
    }

    /**
     * Creates the Clan
     *
     * @param callback
     */
    public void createClan(Runnable callback) {
        SQLManagement.existsClan(tag, exists -> {
            if (!exists) {
                ranks.add(new DefaultMemberRank());
                ranks.add(new DefaultPremiumRank());
                ranks.add(new DefaultModRank());
                ranks.add(new DefaultOwnerRank());
                BungeeBoostrap.instance.dbHandler.createBuilder("INSERT INTO Clans(tag, name, color, level, bankAmount, ranks, state, font) VALUES (?,?,?,?,?,?,?,?)").addObjects(
                        this.tag, this.name, this.color, this.level, this.clanBankAmount, this.serializeRankList(), this.clanState.toString(), this.font.toString()
                ).updateAsync();
                callback.run();
            }
        });
    }

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }

    /**
     * Pushes a change into the Database
     */
    public void pushChange() {
        SQLManagement.setClanValues(this.tag, this.name, this.color, this.level, this.clanBankAmount, serializeRankList(), this.clanState.toString(), font.toString());
        ProxyServer.getInstance().getLogger().info("[CLAN] Clan " + this.tag + " was pushed!");
    }

    public int getMaxPlayers() {
        return (this.level == 1 ? 20 : this.level == 2 ? 50 : this.level == 3 ? 75 : this.level == 4 ? 100 : this.level == 5 ? 150 : 200);
    }

    /**
     * Sends a message with a message key to all Players
     *
     * @param messageKey
     * @param args
     */
    public void sendMessageToAllPlayers(String messageKey, String... args) {
        this.getPlayersAsync(clanPlayers -> {
            for (ClanPlayer player : clanPlayers) {
                player.toProxyPlayer().sendMessage(new TextComponent(AunaAPI.getApi().getMessageService().getMessageForProxiedPlayer(player.toProxyPlayer(), messageKey, args)));
            }
        });
    }

    public String serializeRankList() {
        ClanRank[] rankArray = new ClanRank[this.ranks.size()];
        this.ranks.toArray(rankArray);
        return new Gson().toJson(rankArray);
    }

    public ClanRank getDefaultRank() {
        return getRanks().get(0);
    }

    public void unserializeJsonList(final String serializedJsonContent) {
        ClanRank[] rankArray = new Gson().fromJson(serializedJsonContent, ClanRank[].class);
        this.ranks = Arrays.asList(rankArray);
    }


    public enum ClanState {
        CLOSE, INVITE, OPEN
    }
}
