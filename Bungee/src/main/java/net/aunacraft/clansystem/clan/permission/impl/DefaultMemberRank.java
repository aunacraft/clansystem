package net.aunacraft.clansystem.clan.permission.impl;

import net.aunacraft.clansystem.clan.permission.ClanRank;
import net.aunacraft.clansystem.clan.permission.provider.PermissionProvider;
import net.md_5.bungee.api.ChatColor;

public class DefaultMemberRank extends ClanRank {
    /**
     * Initializes DefaultRank
     */
    public DefaultMemberRank() {
        super(new PermissionProvider(), 1, "member", "Member", ChatColor.GRAY.toString());
        this.getPermissionProvider().insertDefaults();
    }
}
