package net.aunacraft.clansystem.clan.mysql;

import net.aunacraft.clansystem.BungeeBoostrap;

import java.util.function.Consumer;

public class SQLManagement {

    /**
     * Gets the ClanPlayer Table values Async
     *
     * @param uuid
     * @param callback
     */
    public static void getValueFromClanPlayerAsync(final String uuid, final Consumer<String[]> callback) {
        BungeeBoostrap.instance.dbHandler.createBuilder("SELECT * FROM clan_player WHERE uuid = ?").addObjects(uuid).queryAsync(rs -> {
            try {
                if (rs.next()) {
                    final String[] values = new String[2];
                    values[0] = rs.getString("tag");
                    values[1] = rs.getString("rank");
                    callback.accept(values);
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });
    }

    /**
     * Gets the Clan Table values Async
     *
     * @param tag
     * @param callback
     */
    public static void getClanValuesByClanTag(final String tag, final Consumer<String[]> callback) {
        BungeeBoostrap.instance.dbHandler.createBuilder("SELECT * FROM Clans WHERE tag = ?").addObjects(tag).queryAsync(rs -> {
            try {
                if (rs.next()) {
                    final String[] valueArray = new String[7];
                    valueArray[0] = rs.getString("name");
                    valueArray[1] = rs.getString("color");
                    valueArray[2] = "" + rs.getInt("level");
                    valueArray[3] = "" + rs.getLong("bankAmount");
                    valueArray[4] = rs.getString("ranks");
                    valueArray[5] = rs.getString("state");
                    valueArray[6] = rs.getString("font");
                    callback.accept(valueArray);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Sets the Clan Values to the table
     *
     * @param tag
     * @param name
     * @param color
     * @param level
     * @param bankAmount
     * @param ranks
     * @param state
     * @param font
     */
    public static void setClanValues(final String tag, final String name, final String color, final int level, final long bankAmount, final String ranks, final String state, final String font) {
        setClanValue("tag", tag, tag);
        setClanValue("name", tag, name);
        setClanValue("color", tag, color);
        setClanValue("level", tag, level);
        setClanValue("bankAmount", tag, bankAmount);
        setClanValue("ranks", tag, ranks);
        setClanValue("state", tag, state);
        setClanValue("font", tag, font);
    }

    /**
     * Sets the ClanPlayer Values to the table
     *
     * @param uuid
     * @param value
     * @param rankName
     */
    public static void setClanPlayerValues(final String uuid, final String value, final String rankName) {
        BungeeBoostrap.instance.dbHandler.createBuilder("UPDATE clan_player SET tag = ?,rank = ? WHERE uuid = ?").addObjects(value, rankName, uuid).updateAsync();
        //ClanSystem.getInstance().getDatabaseHandler().createBuilder("UPDATE clan_player SET rank = ? WHERE uuid = ?").addObjects(rankName, uuid).updateAsync();
    }

    /**
     * Sets a Clan value to the table
     *
     * @param key
     * @param tag
     * @param value
     */
    public static void setClanValue(final String key, final String tag, final Object value) {
        BungeeBoostrap.instance.dbHandler.createBuilder("UPDATE Clans SET " + key + " ='?' WHERE tag =?").addObjects(value, tag).updateAsync();
    }

    /**
     * Returns if the ClanPlayer is existing
     *
     * @param uuid
     * @param callback
     */
    public static void existsClanPlayer(final String uuid, final Consumer<Boolean> callback) {
        BungeeBoostrap.instance.dbHandler.createBuilder("SELECT * FROM clan_player WHERE uuid=?").addObjects(uuid).queryAsync(rs -> {
            try {
                callback.accept(rs.next());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Returns if the Clan exists
     *
     * @param tag
     * @param callback
     */
    public static void existsClan(final String tag, final Consumer<Boolean> callback) {
        BungeeBoostrap.instance.dbHandler.createBuilder("SELECT * FROM Clans WHERE tag = ?").addObjects(tag).queryAsync(rs -> {
            try {
                callback.accept(rs.next());
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });
    }

}
