package net.aunacraft.clansystem.clan.invites;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.clansystem.BungeeBoostrap;
import net.aunacraft.clansystem.clan.Clan;
import net.aunacraft.clansystem.clan.ClanPlayer;
import net.aunacraft.clansystem.clan.mysql.SQLManagement;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.api.message.PluginMessage;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class InviteSession implements Listener {

    private final ClanPlayer maintainer;
    private final String playerToInvite;
    private InviteSessionState currentState;

    /**
     * Initializes the InviteSession
     *
     * @param maintainer
     * @param nameOfPlayerToInvite
     * @param state
     */
    public InviteSession(ClanPlayer maintainer, String nameOfPlayerToInvite, InviteSessionState state) {
        this.maintainer = maintainer;
        this.playerToInvite = nameOfPlayerToInvite;
        this.currentState = state;
        System.out.println("2");
    }

    /**
     * Starts the Session
     */
    public void start() {
        System.out.println("3");
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(this.playerToInvite);
        /*
            Request if Player is online
         */
        System.out.println("4");
        if (player != null) {
            System.out.println("5");
            ProxyServer.getInstance().getPluginManager().registerListener(BungeeBoostrap.instance, this);
            SQLManagement.existsClanPlayer(player.getUniqueId().toString(), exists -> {
                System.out.println("6");
                if (exists) {
                    /**
                     * Tells the Player that he has to leave the Clan
                     */
                    this.maintainer.toAunaPlayer().toProxiedPlayer().sendMessage(new TextComponent(AunaAPI.getApi().getMessageService().getMessageForProxiedPlayer(this.maintainer.toProxyPlayer(), "clan.message.havetoleaveclan")));
                    this.currentState = InviteSessionState.CANCELLED;
                    stop();
                } else {
                    System.out.println("7");
                    /**
                     * Starts player Request executed by a command
                     */
                    String clanTag = maintainer.getClan().getTag();
                    TextComponent text = new TextComponent(AunaAPI.getApi().getMessageService().getMessageForProxiedPlayer(player, "clan.invitesession.requestmsg1", maintainer.toAunaPlayer().getName(), maintainer.getClan().getName()));
                    text.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("§aAccept")));
                    text.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "clan accept " + clanTag));
                    player.sendMessage(text);
                    player.sendMessage(new TextComponent(AunaAPI.getApi().getMessageService().getMessageForProxiedPlayer(player, "clan.invitesession.requestmsg2", clanTag)));
                    this.currentState = InviteSessionState.WAITING;
                    System.out.println("8");
                }
            });
        } else {
            maintainer.toAunaPlayer().toProxiedPlayer().sendMessage(new TextComponent(AunaAPI.getApi().getMessageService().getMessageForProxiedPlayer(this.maintainer.toAunaPlayer().toProxiedPlayer(), "clan.session.notonline")));
        }
    }

    /**
     * Stops the Session
     */
    public void stop() {
        ProxyServer.getInstance().getPluginManager().unregisterListener(this);
        if (ProxyServer.getInstance().getPlayer(this.playerToInvite) != null) {
            PluginMessage pluginMessage = new PluginMessage("clansystem:inviteRequest");
            pluginMessage.set("invitedPlayer", this.playerToInvite);
            pluginMessage.set("sessionstate", this.currentState.toString());
            AunaCloudAPI.getMessageAPI().sendPluginMessage(ProxyServer.getInstance().getPlayer(this.playerToInvite).getServer().getInfo().getName(), pluginMessage);
        } else {
            throw new NullPointerException("Player not online");
        }
    }

    @EventHandler
    public void handleChatInput(ChatEvent event) {
        if (event.getSender() instanceof ProxiedPlayer) {
            if (event.getMessage().startsWith("/clan accept")) {
                String[] splittedMessage = event.getMessage().split(" ");
                ProxiedPlayer player = (ProxiedPlayer) event.getSender();
                if (splittedMessage.length == 3) {
                    if (splittedMessage[2].equalsIgnoreCase(this.maintainer.getClan().getTag()) && this.currentState.equals(InviteSessionState.WAITING)) {
                        Clan toInviteIn = maintainer.getClan();
                        ClanPlayer clanPlayer = new ClanPlayer(toInviteIn.getTag(), player.getUniqueId().toString(), toInviteIn.getDefaultRank().getGroupName());
                        clanPlayer.createClanPlayer();
                        this.currentState = InviteSessionState.ACCEPTED;
                        this.maintainer.getClan().sendMessageToAllPlayers("clan.message.playerjoined", player.getName());
                    } else {
                        player.sendMessage(new TextComponent(AunaAPI.getApi().getMessageService().getMessageForProxiedPlayer(player, "clan.message.dontgotinvite")));
                    }
                } else {
                    player.sendMessage(new TextComponent(AunaAPI.getApi().getMessageService().getMessageForProxiedPlayer(player, "clan.message.usage", "/clan accept <Clan-Tag>")));
                }
            } else if (event.getMessage().startsWith("/clan deny")) {
                String[] splittedMessage = event.getMessage().split(" ");
                ProxiedPlayer player = (ProxiedPlayer) event.getSender();
                if (splittedMessage.length == 3) {
                    if (splittedMessage[2].equalsIgnoreCase(this.maintainer.getClan().getTag()) && this.currentState.equals(InviteSessionState.WAITING)) {
                        Clan toInviteIn = maintainer.getClan();
                        ClanPlayer clanPlayer = new ClanPlayer(toInviteIn.getTag(), player.getUniqueId().toString(), toInviteIn.getDefaultRank().getGroupName());
                        clanPlayer.createClanPlayer();
                        this.currentState = InviteSessionState.DENIED;
                        stop();
                    } else {
                        player.sendMessage(new TextComponent(AunaAPI.getApi().getMessageService().getMessageForProxiedPlayer(player, "clan.message.dontgotinvite")));
                    }
                } else {
                    player.sendMessage(new TextComponent(AunaAPI.getApi().getMessageService().getMessageForProxiedPlayer(player, "clan.message.usage", "/clan deny <Clan-Tag>")));
                }
            }
        }
    }

}
