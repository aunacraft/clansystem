package net.aunacraft.clansystem.clan;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import lombok.Setter;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.clansystem.BungeeBoostrap;
import net.aunacraft.clansystem.clan.mysql.SQLManagement;
import net.aunacraft.clansystem.clan.permission.provider.ClanPermission;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.UUID;
import java.util.function.Consumer;

@Getter
@Setter
public class ClanPlayer {

    private String clanKey;
    private String uuid;
    private String rankName;

    /**
     * Gets ClanPlayer-Class Instance (Used for creating new ClanPlayers)
     *
     * @param clanKey
     * @param uuid
     * @param rankName
     */
    public ClanPlayer(final String clanKey, final String uuid, final String rankName) {
        this.clanKey = clanKey;
        this.uuid = uuid;
        this.rankName = rankName;
    }

    /**
     * Gets ClanPlayer from Database
     *
     * @param uuid
     * @param playerConsumer
     */
    public ClanPlayer(final String uuid, final Consumer<ClanPlayer> playerConsumer) {
        this.uuid = uuid;
        SQLManagement.getValueFromClanPlayerAsync(uuid, value -> {
            clanKey = value[0];
            rankName = value[1];
            if (playerConsumer != null) playerConsumer.accept(this);
        });
    }

    public static ClanPlayer fromString(final String from) {
        return new Gson().fromJson(from, ClanPlayer.class);
    }

    /**
     * Creates the Clan Player (Sets the values to the Database)
     */
    public void createClanPlayer() {
        if (clanKey != null && uuid != null && this.rankName != null) {
            SQLManagement.existsClanPlayer(this.uuid, exists -> {
                if (!exists)
                    BungeeBoostrap.instance.dbHandler.createBuilder("INSERT INTO clan_player(uuid, tag, rank) VALUES (?, ?, ?)").addObjects(this.uuid, this.clanKey, this.rankName).updateAsync();
                else
                    ProxyServer.getInstance().getLogger().warning("[CLAN] Failed to Create Clan! Clan Already exists!");
            });
        } else throw new NullPointerException();
    }

    public void delete() {
        BungeeBoostrap.instance.dbHandler.createBuilder("DELETE FROM clan_player WHERE uuid = ?").addObjects(this.uuid).updateAsync();
    }

    /**
     * Pashes value changes to the Database
     */
    public void pushChange() {
        SQLManagement.existsClanPlayer(this.uuid, exists -> {
            if (exists)
                SQLManagement.setClanPlayerValues(this.uuid, this.clanKey, this.rankName);
            else
                ProxyServer.getInstance().getLogger().warning("[CLAN] Failed to push Changes! Clan-Player doesnt exist!");
        });
    }

    /**
     * Permission has request
     *
     * @param permission
     * @return if has permission
     */
    public boolean hasPermission(ClanPermission permission) {
        return this.getClan().getRankFromName(this.rankName).getPermissionProvider().hasPermission(permission);
    }

    /**
     * Permission has request
     *
     * @param permission
     * @param cachedClan
     * @return if has permission
     */
    public boolean hasPermission(ClanPermission permission, Clan cachedClan) {
        return cachedClan.getRankFromName(this.rankName).getPermissionProvider().hasPermission(permission);
    }

    public ProxiedPlayer toProxyPlayer() {
        return this.toAunaPlayer().toProxiedPlayer();
    }

    public Clan getClan() {
        return new Clan(this.clanKey, null);
    }

    public AunaPlayer toAunaPlayer() {
        return AunaAPI.getApi().getPlayer(UUID.fromString(this.uuid));
    }

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
