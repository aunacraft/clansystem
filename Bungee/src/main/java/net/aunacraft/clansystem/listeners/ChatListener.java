package net.aunacraft.clansystem.listeners;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.clansystem.clan.ClanPlayer;
import net.aunacraft.clansystem.clan.mysql.SQLManagement;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.Locale;

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(ChatEvent event) {
        String msg = event.getMessage();
        if (msg.toLowerCase(Locale.ROOT).startsWith("@clan")) {
            if (event.getSender() instanceof ProxiedPlayer) {
                ProxiedPlayer player = (ProxiedPlayer) event.getSender();
                SQLManagement.existsClanPlayer(player.getUniqueId().toString(), exists -> {
                    if (exists) {
                        new ClanPlayer(player.getUniqueId().toString(), clanPlayer -> {
                            clanPlayer.getClan().sendMessageToAllPlayers("§b§lCLAN-CHAT §8-> §6" + player.getName() + ": §f" + msg.replace("@clan", ""));
                            event.setCancelled(true);
                        });
                    } else {
                        player.sendMessage(new TextComponent(AunaAPI.getApi().getMessageService().getMessageForProxiedPlayer(player, "clan.chat.notinclan")));
                        event.setCancelled(true);
                    }
                });
            } else return;
        }
    }

}
