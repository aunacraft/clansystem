package net.aunacraft.clansystem.listeners;

import net.aunacraft.clansystem.clan.Clan;
import net.aunacraft.clansystem.clan.ClanPlayer;
import net.aunacraft.clansystem.clan.invites.InviteSession;
import net.aunacraft.clansystem.clan.invites.InviteSessionState;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.api.message.PluginMessage;

public class PluginMessageListener {

    public PluginMessageListener() {
        AunaCloudAPI.getMessageAPI().registerListener(this::handlePluginMessage);
    }

    public void handlePluginMessage(PluginMessage pluginMessage) {
        if (pluginMessage.getType().equalsIgnoreCase("clansystem:inviteSession")) {
            System.out.println("1");
            new InviteSession(ClanPlayer.fromString(pluginMessage.getString("sessionowner")), pluginMessage.getString("invitedPlayer"), InviteSessionState.valueOf(pluginMessage.getString("sessionstate")))
                    .start();
        }
        if (pluginMessage.getType().equalsIgnoreCase("clansystem:broadcast")) {
            if (!pluginMessage.getString("message").contains("|"))
                new Clan(pluginMessage.getString("clan"), clan -> clan.sendMessageToAllPlayers(pluginMessage.getString("message")));
            else {
                String[] args = pluginMessage.getString("message").split("|");
                String[] data = new String[args.length];
                for (int i = 1; i < args.length; i++) data[i - 1] = args[i];
                new Clan(pluginMessage.getString("clan"), clan -> clan.sendMessageToAllPlayers(args[0], data));
            }
        }
    }
}
